MSG=@
IGNORE_DOT_FILES=. .. .git .gitmodules .gitignore .config
DOT_FILES=$(filter-out $(IGNORE_DOT_FILES), $(wildcard .*) .ssh/config $(wildcard .config/*))
DOT_INSTALLED=$(foreach dot, $(DOT_FILES), $(HOME)/$(dot))
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

PKG_ARCH_BASE=aspell-en aspell-fr conky cmake cscope ctags dunst eog firefox flameshot noto-fonts-emoji fzf git gmrun gnome-terminal genius gvim hunspell-fr hyphen-en hyphen-fr lightdm numlockx openssh polkit-gnome python-language-server python3 python-pynvim sudo tint2 tmux unrar unzip udevil zsh xclip openbox obconf
PKG_ARCH_DEV=autopep8 bear cppcheck clang flake8 gcc hstr shellcheck strace gitk tig

PKG_UBUNTU_BASE=aspell-en aspell-fr cmake conky cscope exuberant-ctags dunst eog firefox flameshot git gmrun gnome-terminal genius vim-gtk3 hunspell-fr hyphen-fr lightdm numlockx python3-pynvim python3 sudo tint2 tmux unrar unzip udevil unifont zsh openbox obconf

PKG_UBUNTU_DEV=bear cppcheck clang flake8 gcc shellcheck strace gnupg flex bison build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 libncurses5-dev lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip fontconfig clangd-10 clang-tools gitk tig android-sdk-platform-tools-common cmake-curses-gui git-lfs

ifeq ($(USER), root)
$(info Should not be run as root)
$(info Put youself in wheel group and add \"%wheel ALL=(ALL) NOPASSWD: ALL\" to /etc/sudoers)
$(error Should not be run as root)
endif

all: conf

conf:$(DOT_INSTALLED)
	git submodule update --init

%_all: conf %_install_base %_install_dev
	vim -c :PluginInstall -c :xa

$(HOME)/%: $(ROOT_DIR)/%
	$(MSG)echo "installing $@"
	$(MSG)ln -s $< $@

.ONESHELL:
/usr/bin/yay:
	sudo pacman -Sy --needed git base-devel
	git clone https://aur.archlinux.org/yay.git
	cd yay && makepkg -s -i
	cd ..
	rm -rf yay

ubuntu_install_base:
	$(info going to install $(PKG_UBUNTU_BASE))
	sudo apt install -y $(PKG_UBUNTU_BASE)

ubuntu_install_dev:
	$(info going to install $(PKG_UBUNTU_DEV))
	sudo apt install -y $(PKG_UBUNTU_DEV)

arch_install_base: /usr/bin/yay
	$(info going to install $(PKG_ARCH_BASE))
	yay -Sy --needed  $(PKG_ARCH_BASE)

arch_install_dev: /usr/bin/yay
	$(info going to install $(PKG_ARCH_DEV))
	yay -Sy --needed  $(PKG_ARCH_DEV)


udevil_install: arch_install_base
ifneq (,$(wildcard /etc/systemd/system/multi-user.target.wants/devmon@$(USER).service))
	$(MSG)echo "udevil already installed"
else
	sudo systemctl enable devmon@$(USER)
	sudo systemctl start devmon@$(USER)
endif
