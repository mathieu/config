#export http_proxy="http://grp-horus:3128" 
#export https_proxy="http://grp-horus:3128"
#export ftp_proxy="http://grp-horus:3128"

#Java And Android Env
export JAVA_HOME=/usr/lib/jvm/java-6-sun-1.6.0.12
export ANDROID_HOME=$HOME/Android/android-sdk-linux_86
export ANDROID_SKD=$ANDROID_HOME/platforms/android-1.6/
export ECLIPSE_HOME=$HOME/Tools/eclipse
export NETBEANS_HOME=$HOME/netbeans-6.8
export OOFICE_HOME=/opt/openoffice.org3/program
export PATH=$NETBEANS_HOME/bin:$JAVA_HOME/bin/:$ANDROID_HOME/tools:$ANDROID_SKD/tools:$ECLIPSE_HOME:$OOFICE_HOME:$PATH

#Perso Bin
export PATH=$HOME/bin/bin:$HOME/bin:$PATH

# BitBake Env
export OEBASE=/home/mathieu/OpenEmbedded
export BBPATH=$OEBASE/build:$OEBASE/openembedded
export PATH=$OEBASE/bitbake/bin:$PATH

# Scratchbox
export PATH=/home/mathieu/bin/sb2/bin:$PATH

export CSCOPE_DB=$HOME/cscope/cscope.out
#emacsAndViKeys
#bindkey -v
#bindkey "^P" vi-up-line-or-history
#bindkey "^N" vi-down-line-or-history
#
#bindkey "^[[1~" vi-beginning-of-line   # Home
#bindkey "^[[4~" vi-end-of-line         # End
#bindkey '^[[2~' beep                   # Insert
#bindkey '^[[3~' delete-char            # Del
#bindkey '^[[5~' vi-backward-blank-word # Page Up
#bindkey '^[[6~' vi-forward-blank-word  # Page Down


# make ctrl-r working
#bindkey -M viins '^r' history-incremental-search-backward
#bindkey -M vicmd '^r' history-incremental-search-backward

