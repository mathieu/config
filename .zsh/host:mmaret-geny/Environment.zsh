export ECLIPSE_HOME=
export ANDROID_HOME=$HOME/local/adt/sdk
export NDK_HOME=$HOME/Android/ndk/android-ndk-r10e/
export MY_PROJECT=$HOME/Projects/genymotion
export MY_PROJECT_TOOLCHAIN=$MY_PROJECT/src/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.7/bin
#export JAVA_HOME=$HOME/local/jdk1.7.0_21
export GENYMOTION_HOME=$HOME/local/genymotion
export DEBEMAIL=mmaret@genymobile.com
export DEBFULLNAME=Mathieu Maret
export PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools/:$ANDROID_HOME/build-tools/android-4.2.2:$NDK_HOME:$GENYMOTION_HOME:$PATH:$MY_PROJECT_TOOLCHAIN

source /etc/profile.d/vte.sh
export CCACHE_DIR=/backup/mmaret/ccache/
