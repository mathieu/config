#export http_proxy="http://grp-horus:3128" 
#export https_proxy="http://grp-horus:3128"
#export ftp_proxy="http://grp-horus:3128"

#Java And Android Env
export ECLIPSE_HOME=/home/mathieu/eclipse/adt-bundle-linux-x86_64-20131030/eclipse
export ANDROID_HOME=/home/mathieu/eclipse/adt-bundle-linux-x86_64-20131030/sdk

#Perso Bin
export PATH=$HOME/bin/bin:$HOME/bin:$PATH

export EDITOR=/usr/bin/vim
#export http_proxy="http://grp-horus:3128" 
#export https_proxy="http://grp-horus:3128"
#export ftp_proxy="http://grp-horus:3128"
export ANDROID_DIR=/home/mathieu/android/android-sdk-linux_x86-1.5_r1
#export ANDROID_HOME=/opt/android-sdk
export ANDROID_SKD=/opt/android-sdk/platforms/android-4
export ANDROID_NDK=/opt/android-ndk
#export PATH=$ANDROID_DIR/tools:$PATH
export PATH=$ANDROID_HOME/tools:$ANDROID_SKD/tools:$ANDROID_HOME/platform-tools:$ANDROID_NDK:$ECLIPSE_HOME:$PATH



#emacsAndViKeys
#bindkey -v
#bindkey "^P" vi-up-line-or-history
#bindkey "^N" vi-down-line-or-history
#
#bindkey "^[[1~" vi-beginning-of-line   # Home
#bindkey "^[[4~" vi-end-of-line         # End
#bindkey '^[[2~' beep                   # Insert
#bindkey '^[[3~' delete-char            # Del
#bindkey '^[[5~' vi-backward-blank-word # Page Up
#bindkey '^[[6~' vi-forward-blank-word  # Page Down


# make ctrl-r working
#bindkey -M viins '^r' history-incremental-search-backward
#bindkey -M vicmd '^r' history-incremental-search-backward

