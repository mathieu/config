
# couleur dans man
export PAGER="less"
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# X : Disables sending the termcap initialization and deinitialization strings to the terminal.
# F : Exit if the entire file can be displayed on the first screen
# R : couleur

export LESS="$LESS -XRF"
#export GIT_PAGER="more"

export EDITOR=/usr/bin/vim

if [[ -z "$BROWSER" ]] ; then
    if [[ -n "$DISPLAY" ]] ; then
        #v# If X11 is running
        export BROWSER=x-www-browser
    else
        #v# If no X11 is running
        export BROWSER=links
    fi
fi

export PATH=$HOME/local/bin:$PATH:$HOME/.cargo/bin
