#man zshzle
#man zshcontrib
# Lines configured by zsh-newuser-install
export HISTFILE=~/.histfile
export HISTSIZE=1000000
export SAVEHIST=1000000
export HISTTIMEFORMAT="%h/%d - %H:%M:%S "
#export HISTCONTROL=ignoredups
export HISTCONTROL=erasedups

setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
#setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
#setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
#setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
#setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
#setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
#setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
#setopt HIST_BEEP                 # Beep when accessing nonexistent history.


#export HISTIGNORE=”&:ls:[bf]g:exit”
setopt -h histappend
PROMPT_COMMAND='history -a'

setopt append_history autocd extendedglob nomatch
setopt hist_ignore_dups # ignore duplication command history list
setopt hist_ignore_space # ignore line starting by a space
#setopt share_history # share command history data

# Use 'cat -v' to obtain the keycodes
bindkey "\C-b" backward-word         ## ctrl-b
bindkey "\C-f" forward-word          ## ctrl-f
bindkey "^[[3~" delete-char          ## Del
bindkey "^[[7~" beginning-of-line    ## Home
bindkey "^[[8~" end-of-line          ## End
bindkey " " magic-space              ## do history expansion on space (after ! or ^)
bindkey '^[[1;5C' forward-word                        # [Ctrl-RightArrow] - move forward one word
bindkey '^[[1;5D' backward-word                       # [Ctrl-LeftArrow] - move backward one word

#History on Key Up/Down
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down
#bindkey "^[[A" history-search-backward
#bindkey "^[[B" history-search-forward
#bindkey "^[[A" up-line-or-search     ## up arrow for back-history-search
#bindkey "^[[B" down-line-or-search   ## down arrow for fwd-history-search

#edit current cmdline in vim with Ctrl-x Ctrl-e
autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

unsetopt beep
#force emacs binding for tmux && screen
bindkey -e

# Completion for bash fonctions
autoload -U bashcompinit
bashcompinit

# Correct for wrong command like sl
setopt correct
#correct arg as well
#setopt correctall

# Activate Prompt
autoload -U promptinit
promptinit
#prompt adam1
prompt bart


# pushd pour cd
setopt  autopushd
setopt pushd_ignore_dups
# ne tue pas les processus quand zsh quitte
#setopt  nohup
# auto cd avec le nom du dossier
setopt  autocd

#Un grep avec des couleurs :
#export GREP_COLOR=31
#alias grep='grep --color=auto'
GREP_OPT="--color=auto"

# avoid VCS folders (if the necessary grep flags are available)
grep-flag-available() {
    echo | grep $1 "" >/dev/null 2>&1
}
if grep-flag-available --exclude-dir=.cvs; then
    for PATTERN in .cvs .git .hg .svn; do
        GREP_OPT+=" --exclude-dir=$PATTERN"
    done
elif grep-flag-available --exclude=.cvs; then
    for PATTERN in .cvs .git .hg .svn; do
        GREP_OPT+=" --exclude=$PATTERN"
    done
fi
unfunction grep-flag-available

#export GREP_OPTIONS="$GREP_OPTIONS"
export GREP_COLOR='mt=1;31'
alias grep="grep $GREP_OPT"



# mimes type support e.g: ./toto.pdf
# /!\ zsh-mime-setup is slow !
#autoload -U zsh-mime-setup
#autoload -U zsh-mime-handler
#zsh-mime-setup

if [ -f "${HOME}/.gpg-agent-info" ]; then
    . "${HOME}/.gpg-agent-info"
    export GPG_AGENT_INFO
    export SSH_AUTH_SOCK
    export SSH_AGENT_PID
fi

