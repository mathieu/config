#Search on Google
google()  {
	emulate -L zsh
	${=BROWSER} "http://www.google.com/search?&num=100&q=$*"
}

#Search French Wiktionary
wikdic_fr()  {
    emulate -L zsh
    ${=BROWSER} http://fr.wiktionary.org/wiki/${(C)1// /_}
}
#Search English  Wiktionary
wikdic_en()  {
    emulate -L zsh
    ${=BROWSER} http://en.wiktionary.org/wiki/${(C)1// /_}
}

#Search French Wikipedia
wiki_fr()  {
    emulate -L zsh
    ${=BROWSER} "http://fr.wikipedia.org/w/index.php?title=Special%3ASearch&search=$*"
}
#Search English Wikipedia
wiki_en()  {
    emulate -L zsh
    ${=BROWSER} "http://en.wikipedia.org/w/index.php?title=Special%3ASearch&search=$*"
}

#French conjugaison
verbe() {
    emulate -L zsh
    ${=BROWSER} "http://www.la-conjugaison.fr/du/verbe/${*}.php"
}

weather() {
    emulate -L zsh
    [[ -n "$1" ]] || {
        print 'Usage: weather <station_id>' >&2
        print 'List of stations: http://en.wikipedia.org/wiki/List_of_airports_by_ICAO_code'>&2
        return 1
    }

    local VERBOSE="yes"    # TODO: Make this a command line switch

    local ODIR=`pwd`
    local PLACE="${1:u}"
    local DIR="${HOME}/.weather"
    local LOG="${DIR}/log"

    [[ -d ${DIR} ]] || {
        print -n "Creating ${DIR}: "
        mkdir ${DIR}
        print 'done'
    }

    print "Retrieving information for ${PLACE}:"
    print
    cd ${DIR} && wget -T 10 --no-verbose --output-file=$LOG --timestamping http://weather.noaa.gov/pub/data/observations/metar/decoded/$PLACE.TXT

    if [[ $? -eq 0 ]] ; then
        if [[ -n "$VERBOSE" ]] ; then
            cat ${PLACE}.TXT
        else
            DATE=$(grep 'UTC' ${PLACE}.TXT | sed 's#.* /##')
            TEMPERATURE=$(awk '/Temperature/ { print $4" degree Celcius / " $2" degree Fahrenheit" }' ${PLACE}.TXT | tr -d '(')
            echo "date: $DATE"
            echo "temp:  $TEMPERATURE"
        fi
    else
        print "There was an error retrieving the weather information for $PLACE" >&2
        cat $LOG
        cd $ODIR
        return 1
    fi
    cd $ODIR
}

battery() {
if [[ $BATTERY -gt 0 ]] ; then
    PERCENT="${${"$(acpi 2>/dev/null)"}/(#b)[[:space:]]#Battery <->: [^0-9]##, (<->)%*/${match[1]}}"
    if [[ -z "$PERCENT" ]] ; then
        PERCENT='acpi not present'
    else
        if [[ "$PERCENT" -lt 20 ]] ; then
            PERCENT="warning: ${PERCENT}%%"
        else
            PERCENT="${PERCENT}%%"
        fi
    fi
fi
}

repofind () {
    find . -name .repo -prune -o -name .git -prune -o "$@" -print0
}

mgrep () {
    find . -name .repo -prune -o -name .git -prune -o -path ./out -prune -o -regextype posix-egrep -iregex '(.*\/Makefile|.*\/Makefile\..*|.*\.make|.*\.mak|.*\.mk|CMakeLists.txt)' -type f -print0 | xargs -0 grep --color -n "$@"
}

bbgrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f \( -name '*.bb' -o -name '*.bbclass' -o -name '*.bbappend' \) -print0 | xargs -0 grep --color -n "$@"
}

cgrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f \( -name '*.c' -o -name '*.cc' -o -name '*.cpp' -o -name '*.h' -o -name '*.hpp' -o -name "*.ino" \) -print0 | xargs -0 grep --color -n "$@"
}

hgrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f \( -name '*.h' -o -name '*.hpp' \) -print0 | xargs -0 grep --color -n "$@"
}
# Echo NULL separeted file if they exist
displayExist(){
    while IFS= read -r -d '' f; do
        test -e "$f" && echo -n $f'\0'
    done
}

cogrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f \( -name '*.o' ! -name '*.mod.o' \) -print0 | sed 's/.o\x0/.c\x0/g'| displayExist | xargs -0 grep --color -n "$@"
}

jgrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f -name "*\.java" -print0 | xargs -0 grep --color -n "$@"
}

pygrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f -name "*\.py" -print0 | xargs -0 grep --color -n "$@"
}

cjgrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f \( -name '*.c' -o -name '*.cc' -o -name '*.cpp' -o -name '*.h' -o -name '*.hpp' -o -name "*.java" \) -print0 | xargs -0 grep --color -n "$@"
}

resgrep () {
    for dir in `find . -name .repo -prune -o -name .git -prune -o -name res -type d`
        do
            find $dir -type f -name '*\.xml' -print0 | xargs -0 grep --color -n "$@"
        done
}

sgrep(){
    find . -name .repo -prune -o -name .git -prune -o  -type f -iregex '.*\.\(c\|h\|cc\|cpp\|hpp\|S\|java\|xml\|sh\|mk\|aidl\|vts\)' \
        -exec grep --color -n "$@" {} +
}


mangrep() {
        find . -name .repo -prune -o -name .git -prune -o -path ./out -prune -o -type f -name 'AndroidManifest.xml' -print0 | xargs -0 grep --color -n "$@"
}

sepgrep() {
        find . -name .repo -prune -o -name .git -prune -o -path ./out -prune -o -name sepolicy -type d -print0 | xargs -0 grep --color -n -r --exclude-dir=\.git "$@"
}


docgrep () {
    find . -name .repo -prune -o -name .git -prune -o -type f \( -name '*.doc' -o -name '*.docx' -o -name '*.pdf' -o -name '*.odt'  \) -print0 | xargs -i -0 docgrepfile {} "$@"
}

ggrep()
{
    find . -name .repo -prune -o -name .git -prune -o -name out -prune -o -type f -name "*\.gradle" \
        -exec grep --color -n "$@" {} +
}

gogrep()
{
    find . -name .repo -prune -o -name .git -prune -o -name out -prune -o -type f -name "*\.go" \
        -exec grep --color -n "$@" {} +
}

mgrep()
{
    find . -name .repo -prune -o -name .git -prune -o -path ./out -prune -o \( -regextype posix-egrep -iregex '(.*\/Makefile|.*\/Makefile\..*|.*\.make|.*\.mak|.*\.mk|.*\.bp)' -o -regextype posix-extended -regex '(.*/)?(build|soong)/.*[^/]*\.go' \) -type f \
        -exec grep --color -n "$@" {} +
}

treegrep()
{
    find . -name .repo -prune -o -name .git -prune -o -regextype posix-egrep -iregex '.*\.(c|h|cpp|hpp|S|java|xml)' -type f \
        -exec grep --color -n -i "$@" {} +
}

findduplicate ()
{
    find . -name .repo -prune -o -name .git -prune -o -not -empty -type f -printf "%s\n" | sort -rn | uniq -d | xargs -I{} -n1 find -type f -size {}c -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=separate
}

gettop ()
{
    local TOPFILE=$1;
    if [ -n "$TOP" -a -f "$TOP/$TOPFILE" ]; then
        echo $TOP;
    else
        if [ -f $TOPFILE ]; then
            PWD= /bin/pwd;
        else
            local HERE=$PWD;
    T=;
    while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ]; do
        \cd ..;
    T=`PWD= /bin/pwd`;
    done;
    \cd $HERE;
    if [ -f "$T/$TOPFILE" ]; then
        echo $T;
    fi;
    fi;
    fi
}

crootgit ()
{
    local TOPFILE=.git/config;
    T=$(gettop $TOPFILE);
    if [ "$T" ]; then
        \cd $(gettop $TOPFILE);
    else
        echo "Couldn't locate the top of the tree.  Try setting TOP.";
    fi
}

croot ()
{
    local TOPFILE=build/core/envsetup.mk;
    T=$(gettop $TOPFILE);
    if [ "$T" ]; then
        \cd $(gettop $TOPFILE);
    else
        echo "Couldn't locate the top of the tree.  Try setting TOP.";
    fi
}

syswrite ()
{
    adb wait-for-device && adb root || return 1;
    if [[ -n $(adb disable-verity | grep -i "reboot") ]]; then
        echo "rebooting";
        adb reboot && adb wait-for-device && adb root || return 1;
    fi;
    adb wait-for-device && adb remount || return 1
}

function zsh_stats() {
  fc -l 1 | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n20
}

function secure_chromium {
    echo "make sure to have run \"ssh -TND 4711 $USER@host\" before"
    port=4711
    export SOCKS_SERVER=localhost:$port
    export SOCKS_VERSION=5
    chromium-browser &
    exit
}

apush() {
    adb root && adb wait-for-device && adb remount && adb wait-for-device &&  adb push $1 $2
}

sudocker(){
    DOCKER_EXEC_ARGS=${DOCKER_EXEC_ARGS:--u 0}
    sdocker $@
}

sdocker (){
    DOCKER_SSH_AUTH=$HOME/docker_ssh_auth
    AOSP_IMAGE=${AOSP_IMAGE:-$HOME/Sources/android}

    if [ $# -eq 0 ]; then
        echo "Usage: $0 DOCKER_IMAGE_NAME"
        return
    fi

    if [ -n "$SSH_AUTH_SOCK" ]; then
        $(ls -l $HOME/docker_ssh_auth 2> /dev/null | grep $SSH_AUTH_SOCK > /dev/null)
        if [ $? ];
        then
            unlink $DOCKER_SSH_AUTH > /dev/null
            ln -s $SSH_AUTH_SOCK $DOCKER_SSH_AUTH
        fi
        SSH_AUTH_ARGS="-v $DOCKER_SSH_AUTH:/tmp/ssh_auth -e SSH_AUTH_SOCK=/tmp/ssh_auth"
    fi
    AOSP_ARGS=${AOSP_ARGS:--it}
    img=$(docker ps | grep $1 | awk '{print $1}');
    if [ -z $img ]; then
        details=$(docker ps -a | grep -w $1 | head -1);
        img=$(echo ${details} | awk '{print $1}')
        img_name=$(echo ${details} | awk '{print $2}')
        if [ -z $img ]; then
            echo "Running a new container with image $1"
	        docker run ${AOSP_ARGS} ${=SSH_AUTH_ARGS} -h $(hostname) -e DISPLAY=${DISPLAY} --privileged -v /dev/bus/usb:/dev/bus/usb -v /tmp/.X11-unix/:/tmp/.X11-unix/ -v ${AOSP_IMAGE}:/aosp/Project -v $HOME/Sources/:/aosp/Sources/ -v /local/:/local/ --net host $1
        else
            echo "Starting $img|$img_name"
            docker start -i -a $img
        fi
    else
        echo "Attach to $img|$img_name"
        eval docker exec ${DOCKER_EXEC_ARGS} -it $img /bin/bash
    fi
}

_sdocker () {
    local cur
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(compgen -W "$(docker images | tail -n +2 | grep -v "<none>" | awk '{print $1}')" -- ${cur} ) )
}

complete -o default -F _sdocker sdocker
complete -o default -F _sdocker sudocker

_flash_all () {
    local cur
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(compgen -W "$(ls *.img 2>/dev/null)" -- ${cur} ) )
}

complete -o default -F _flash_all flash_all

# Interactive mv
imv() {
  local src dst
  for src; do
    [[ -e $src ]] || { print -u2 "$src does not exist"; continue }
    dst=$src
    vared dst
    [[ $src != $dst ]] && mkdir -p $dst:h && mv -n $src $dst
  done
}

transfer(){ if [ $# -eq 0 ];then echo "No arguments specified.\nUsage:\n  transfer <file|directory>\n  ... | transfer <file_name>">&2;return 1;fi;if tty -s;then file="$1";file_name=$(basename "$file");if [ ! -e "$file" ];then echo "$file: No such file or directory">&2;return 1;fi;if [ -d "$file" ];then file_name="$file_name.zip" ,;(cd "$file"&&zip -r -q - .)|curl --progress-bar --upload-file "-" "http://transfer.sh/$file_name"|tee /dev/null,;else cat "$file"|curl --progress-bar --upload-file "-" "http://transfer.sh/$file_name"|tee /dev/null;fi;else file_name=$1;curl --progress-bar --upload-file "-" "http://transfer.sh/$file_name"|tee /dev/null;fi;}
