alias mv='nocorrect mv'       # no spelling correction on mv
alias cp='nocorrect cp'
alias mkdir='nocorrect mkdir'
alias rm='nocorrect rm'
alias sudo='nocorrect sudo'
alias tm='tmux attach || tmux'
alias zzz='systemctl hibernate'


#alias
alias ls='ls --color=auto'
alias l='ls'
alias ll='ls --color=auto -lh'
alias la='ls --color=auto -lha'
alias lll='ls --color -lh | less -R'
alias lsd="ls -d */"
alias lsg='ls | grep -i '
alias lssize='ls -lh | awk '\''{ print $5,"\t", $9 }'\'' | sort -h '
alias ..='cd ..'
alias ../..='cd ../..'
alias ../../..='cd ../../..'
alias greptag='grep --exclude=tags --exclude=cscope.out'
alias logcat="adb logcat -v time"
alias remount="adb root && adb remount" # use syswrite instead
alias xte='nohup xterm &' # xte lancera un xterm qui ne se fermera pas si on ferme le terminal
alias minicom='minicom -c on'
alias vims='vim --servername vimserver'
alias vimo='vim --servername vimserver --remote-tab'
alias agrep='grep --exclude=.git --exclude-dir=out --exclude-dir=prebuilts --exclude-dir=docs --exclude=tags --exclude=cscope.out'
alias beep='mpv /usr/share/sounds/freedesktop/stereo/complete.oga &> /dev/null &' #sound in package sound-theme-freedesktop
alias gst="vim '+Gedit:' ."
alias lsempty='find . -depth -type d -empty'
alias rmempty='find . -depth -type d -empty -delete'
alias myip="curl -4 icanhazip.com"
alias bc="bc -l"
alias draw="fingerpaint --hint=$'Press any key or click to finish drawing\nImage will be copied to clipboard' -o - | xclip -sel clip -t image/png"

#Extension Alias
alias -s html=$BROWSER
alias -s org=$BROWSER
alias -s php=$BROWSER
alias -s com=$BROWSER
alias -s net=$BROWSER
alias -s png=eog
alias -s jpg=eog
alias -s gif=eog
alias -s sxw=soffice
alias -s doc=soffice
alias -s gz=tar -xzvf
alias -s bz2=tar -xjvf
alias -s java=$EDITOR
alias -s txt=$EDITOR
alias -s PKGBUILD=$EDITOR
alias -s pdf=evince


# command L equivalent to command |less
alias -g L='|less -R'

# command S equivalent to command &> /dev/null &
alias -g S='&> /dev/null &'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias senv='source ~/venv/bin/activate'
alias r='if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) != 'true' ]]; then
    >&2 echo "Not a git repository"
    return 1
else
    cd $(git root)
fi'
