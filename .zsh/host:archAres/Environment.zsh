#export http_proxy="http://grp-horus:3128" 
#export https_proxy="http://grp-horus:3128"
#export ftp_proxy="http://grp-horus:3128"

#Java And Android Env
export ECLIPSE_HOME=/home/mathieu/Outils/eclipse

#Perso Bin
export PATH=$HOME/bin/bin:$HOME/bin:$PATH

export EDITOR=/usr/bin/vim
#export http_proxy="http://grp-horus:3128" 
#export https_proxy="http://grp-horus:3128"
#export ftp_proxy="http://grp-horus:3128"
export ANDROID_DIR=/home/mathieu/android/android-sdk-linux_86
export ANDROID_HOME=/home/mathieu/android/android-sdk-linux_86
export ANDROID_SKD=/home/mathieu/android/android-sdk-linux/platforms/android-1.6
#export PATH=$ANDROID_DIR/tools:$PATH
export JAVA_HOME=/opt/java
export CATALINA_HOME=/usr/share/tomcat6
export PATH=$JAVA_HOME/bin/:$ANDROID_HOME/tools:$ANDROID_SKD/tools:$ECLIPSE_HOME:$PATH



#emacsAndViKeys
#bindkey -v
#bindkey "^P" vi-up-line-or-history
#bindkey "^N" vi-down-line-or-history
#
#bindkey "^[[1~" vi-beginning-of-line   # Home
#bindkey "^[[4~" vi-end-of-line         # End
#bindkey '^[[2~' beep                   # Insert
#bindkey '^[[3~' delete-char            # Del
#bindkey '^[[5~' vi-backward-blank-word # Page Up
#bindkey '^[[6~' vi-forward-blank-word  # Page Down

#bindkey "^[[1~" vi-beginning-of-line   # Home
#bindkey "^[[4~" vi-end-of-line         # End


# make ctrl-r working
#bindkey -M viins '^r' history-incremental-search-backward
#bindkey -M vicmd '^r' history-incremental-search-backward


