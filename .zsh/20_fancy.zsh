
#thx to matthew loar for that!
#if [[ $TERM == "screen" ]]; then
#
#function precmd {
#  #prompt_adam1_precmd
#  echo -ne "\033]83;title zsh\007"
#}
#
#function preexec {
#  local foo="$2 "
#  local bar=${${=foo}[1]}
#  echo -ne "\033]83;title $bar\007"
#}
#
#fi
#
## Titre de la fenêtre d'un xterm
#case $TERM in
#   xterm*)
#       precmd () {print -Pn "\e]0;%n@%m: %~\a"}
#       ;;
#esac


case $TERM in
	*xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term)
		precmd () {
			if [[ -v SSH_CONNECTION ]]; then
				print -Pn "\e]0;[%n@%M][%~]#\a"
			else
				print -Pn "\e]0;[%~]#\a"
			fi
		}
		preexec () {
			if [[ -v SSH_CONNECTION ]]; then
				print -Pn "\e]0;[%n@%M][%~]# (${1:gs/%/%%})\a"
			else
				print -Pn "\e]0;[%~]# (${1:gs/%/%%})\a"
			fi
		}

		;;
	screen)
		precmd () {
			print -Pn "\e]83;title \"$1\"\a"
			if [[ -v SSH_CONNECTION ]]; then
				print -Pn "\e]0;$TERM - (%L) [%n@%M]# [%~]\a"
			else
				print -Pn "\e]0;$TERM - (%L)# [%~]\a"
			fi
		}
		preexec () {
			print -Pn "\e]83;title \"$1\"\a"
			if [[ -v SSH_CONNECTION ]]; then
				print -Pn "\e]0;$TERM - (%L) [%n@%M]# [%~] ($1)\a"
			else
				print -Pn "\e]0;$TERM - (%L) # [%~] ($1)\a"
			fi
		}
		;;
esac
