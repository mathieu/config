export ECLIPSE_HOME=
export ANDROID_HOME=/opt/android-sdk/
export NDK_HOME=/opt/android-ndk/
export MY_PROJECT=$HOME/Projects/genymotion
export MY_PROJECT_TOOLCHAIN=$MY_PROJECT/src/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.7/bin
#export JAVA_HOME=$HOME/local/jdk1.7.0_21
export GENYMOTION_HOME=$HOME/local/genymotion
export DEBEMAIL=mmaret@genymobile.com
export DEBFULLNAME=Mathieu Maret
export PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools/:$ANDROID_HOME/build-tools/android-4.2.2:$NDK_HOME:$GENYMOTION_HOME:$PATH:$MY_PROJECT_TOOLCHAIN:$HOME/local/bin
export PATH=$PATH:/home/mathieu/.local/bin
source /etc/profile.d/vte.sh
#export CCACHE_DIR=/backup/mmaret/ccache/

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_COMPLETION_TRIGGER='²²'
export FZF_DEFAULT_OPTS="--preview='[[ \$(file --mime {}) =~ binary ]] && echo no preview for {}  || (bat --style==numbers --color=always {} || cat {}) 2>/dev/null | head -300' --bind='f3:execute(bat --style=numbers {} || less -f {}),f2:toggle-preview' --preview-window='right:hidden:wrap' --height 80% --reverse"
FD_OPTIONS="--follow --exclude .git"
export FZF_DEFAULT_COMMAND="git ls-files --cached --others --exclude-standard | fd --type f --type l ${FD_OPTIONS}"

