# see http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#index-parameters_002c-zle
function watch_fonction() {
  zle quote-line
  BUFFER="watch -n1 ${BUFFER}"
  zle accept-line
}
zle -N watch_widget watch_fonction
#bindkey '^t' watch_widget
