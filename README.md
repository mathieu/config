# My Dot Files

This repo contains a set of tools and configuration files used for development.
Especially for Android Rom development

gdb init came from : https://github.com/cyrus-and/gdb-dashboard
## Install tool
You can simply do
```
#install dot files
make
#install package as well (archlinux only)
pacman -Sy make sudo
gpasswd -a USER wheel
edit /etc/sudoers
make arch_all
```

## Vim

Vim plugins are managed by vundle. So at first lunch you have to run in vim
```
 :PluginInstall
```

## Dependencies
* conky
* cscope
* ctags
* gmrun
* gnome-screensaver
* gnome-terminal
* gvim
* numlockx
* openssh
* polkit-gnome
* sudo
* tint2
* tmux
* xbacklight
* xfce4-notifyd
* zsh

## More tools
* android-apktool
* android-bash-completion
* android-udev
* archlinux-themes-slim
* aspell-fr
* autopep8
* bear
* cppcheck
* cups
* dialog
* elinks
* eog
* firefox
* flake8
* flashplugin
* fzf
* bat
* fd
* gcc-multilib
* gedit
* gnome-calculator
* gnome-terminal
* hunspell-en
* hunspell-fr
* hyphen-en
* hyphen-fr
* libreoffice-fresh
* libreoffice-fresh-fr
* mutt
* networkmanager-iwd-overlay # To use iwd instead of wpa_supplicant (archlinux specific)
* notification-daemon
* obconf
* odt2txt
* python2-virtualenv
* seahorse
* shellcheck
* strace
* udevil
* unrar
* unzip
* xchat

## dev tools
* cppcheck
* cppcheck git precommit https://github.com/danmar/cppcheck/blob/master/tools/git-pre-commit-cppcheck
* pycscope (to generate cscope file for python) : https://pypi.python.org/pypi/pycscope/
