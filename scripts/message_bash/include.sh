#!/usr/bin/env bash

myfunct() {
  local a=10
  debug "my a variable is $a"
  msg "value of 'a' squared: $(( a * a ))"
}

check_file() {
  if [ ! -f "monfichier.txt" ]
  then
    display_error "File monfichier.txt not found"
  fi
}

display_error() {
  error "$*"
}
