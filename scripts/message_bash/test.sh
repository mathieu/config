#!/usr/bin/env bash
source message.sh || { >&2 printf "Can't load message.sh"; exit 1; }
source include.sh || { >&2 printf "Can't load include.sh"; exit 1; }
myfunct
check_file
debug "We will display a message"
msg "Test Message"
error "This is a simple error message"
exit 0
