#!/usr/bin/env bash

tags=$(git tag)
for tag in $tags;
do
  commitId=$(git rev-list -n 1 $tag)
  echo "==== $tag $commitId===="
  #git show --abbrev $commitId
done
