#!/bin/bash
set -euo pipefail

FILE_NAME=$1
input=$(mktemp -d)

function finish {
    if [[ ! -z "$input" && -e $input ]]; then
        echo "removing $input"
        rm -rf "$input"
    fi
}

trap finish EXIT

unzip "$FILE_NAME" -d "$input"
find "$input" -name "*.gif" -exec  gifsicle --batch -i {} -O3 \;
cur=$(pwd)
cd "${input}"
zip -r "${cur}/$FILE_NAME" -- *
cd "${cur}"


