 #!/bin/bash
 
 REMOTE=$(git remote)
 BRANCH=$(git branch | grep '*' | sed 's/* //')
 
 git push $REMOTE $BRANCH
 
 exit 0
