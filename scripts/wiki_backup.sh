USER=Webapp
PASSWORD=pontault
DATABASE=wikimedia
BACKUP_DIR=/home/mathieu/backup
if [ ! -d $BACKUP_DIR ];
then
	mkdir $BACKUP_DIR
fi
file=$BACKUP_DIR/wiki-$DATABASE-$(date '+%Y-%m-%d').sql.gz
/usr/bin/mysqldump -u $USER --password=$PASSWORD $DATABASE -c | /usr/bin/gzip > $file
#rsync $BACKUP_DIR
scp $file mathieu@tutabaga.hd.free.fr:~/Backups/wiki/
