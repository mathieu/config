#!/bin/bash
#/usr/bin/certbot certonly --agree-tos --quiet --renew-by-default --webroot -w /srv/http -d mathux.org --email mathieu@mathux.org
#/usr/bin/certbot certonly --agree-tos --quiet --renew-by-default --webroot -w /srv/http/letsencrypt -d gitlab.mathux.org --email mathieu@mathux.org
#/usr/bin/certbot certonly --agree-tos --quiet --renew-by-default --webroot -w /srv/http/letsencrypt -d git.mathux.org --email mathieu@mathux.org

/usr/bin/certbot certonly --authenticator dns-gandi --dns-gandi-credentials /etc/letsencrypt/gandi.ini -d mathux.org -d \*.mathux.org --server https://acme-v02.api.letsencrypt.org/directory
# Emby its proxy served by apache
#/usr/bin/openssl pkcs12 -inkey /etc/letsencrypt/live/mathux.org-0001/privkey.pem -in /etc/letsencrypt/live/mathux.org-0001/fullchain.pem -export -out /etc/letsencrypt/live/mathux.org-0001/emby.pfx -passout pass:
#chown emby:emby /etc/letsencrypt/live/mathux.org-0001/emby.pfx
#/usr/bin/certbot certonly --agree-tos --quiet --renew-by-default --webroot -w /srv/http/letsencrypt -d air.mathux.org --email mathieu@mathux.org
#/usr/bin/certbot certonly --agree-tos --quiet --renew-by-default --webroot -w /lib/python3.6/site-packages/homeassistant/components/frontend/www_static/ -d home.mathux.org --email mathieu@mathux.org
#/usr/bin/certbot certonly --agree-tos --quiet --renew-by-default --webroot -w /lib/python3.6/site-packages/homeassistant/components/frontend/www_static/ -d lefevre.mathux.org --email mathieu@mathux.org

