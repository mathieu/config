#!/bin/bash
user=pi
host=tutabaga.hd.free.fr
ps aux | grep autossh | grep $host > /dev/null
res=$?
if [ $res == 1 ]; then
	autossh -M 2222 -f -N -R 65022:localhost:22 $user@$host
fi
