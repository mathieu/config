#!/bin/zsh

is_running=`ps aux| grep gpg-agent| grep -v grep`

if [ $is_running ] ; then
	echo "gpg is running";
	if [ -f "${HOME}/.gpg-agent-info" ]; then
		. "${HOME}/.gpg-agent-info"
		export GPG_AGENT_INFO
		export SSH_AUTH_SOCK
		export SSH_AGENT_PID
	fi
else
	env=`./run_gpg.sh`
	source $env
fi
mutt
