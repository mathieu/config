bin=/home/mathieu/config/scripts/openPortUpnp
#http
${bin} 80 TCP
sleep 2

#ssh
${bin} 22 TCP
sleep 2

#https
${bin} 443 TCP
sleep 2

#smtp
${bin} 587 TCP
sleep 2

#pop3-ssl
${bin} 995 TCP
sleep 2

#smtp
${bin} 25 TCP
sleep 2

#smtp ssl
${bin} 465 TCP
sleep 2

#imap-ssl
${bin} 993 TCP
sleep 2

#imap TLS
${bin} 143 TCP
sleep 2

#subsonic
${bin} 4040 TCP
sleep 2

#Git
${bin} 9418 TCP
sleep 2

#rTorrent
${bin} 49164 TCP
sleep 2
${bin} 49164 UDP
sleep 2
