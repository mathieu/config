#!/bin/bash
set -eo pipefail

#input file should be
#filename_withou_ext;text to be TTS

OUTDIR=out


while read -r input
do
    STRINGNUM=0
    SHORTTMP=
    filename=$(echo $input| cut -d ";" -f 1)
    filename=${filename}.wav
    text=$(echo $input| cut -d ";" -f 2)
    echo "TTS $text into $filename"
    ary=($text)
    for key in "${!ary[@]}" 
    do
        SHORTTMP[$STRINGNUM]="${SHORTTMP[$STRINGNUM]} ${ary[$key]}"
        LENGTH=$(echo ${#SHORTTMP[$STRINGNUM]})
        #echo "word:$key, ${ary[$key]}"
        #echo "adding to: $STRINGNUM"
        if [[ "$LENGTH" -lt "100" ]]; then
            #echo starting new line
            SHORT[$STRINGNUM]=${SHORTTMP[$STRINGNUM]}
        else
            STRINGNUM=$(($STRINGNUM+1))
            SHORTTMP[$STRINGNUM]="${ary[$key]}"
            SHORT[$STRINGNUM]="${ary[$key]}"
        fi
    done

    for key in "${!SHORT[@]}"
    do
        #echo "line: $key is: ${SHORT[$key]}"

        echo "Playing line: $(($key+1)) of $(($STRINGNUM+1))"
        NEXTURL=$(echo ${SHORT[$key]} | xxd -plain | tr -d '\n' | sed 's/\(..\)/%\1/g')

        script -q -c "mpg123 -q \"http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q=$NEXTURL&tl=fr\" --wav tts.wav --stereo"<<ENDDOC /dev/null
ENDDOC
        sox tts.wav -r 48000 -c 1 -b 16 tts_48000.wav
        sox -v 1.5 tts_48000.wav $OUTDIR/$filename
    done

    echo "---------------"
done < "$1"
