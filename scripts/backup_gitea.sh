#!/bin/bash

nb_bk=$(sudo ls /var/lib/gitea/ | grep -E ".*dump.*.zip" |wc -l)

if [ $nb_bk != 0 ]; then
	echo "EXISTING GITEA BACKUP!"
	exit
fi

sudo -u gitea sh -c "cd ~gitea; gitea dump -c /etc/gitea/app.ini > /dev/null 2>&1"

backup_file=$(sudo ls /var/lib/gitea/ | grep -E ".*dump.*.zip")
backup_file="/var/lib/gitea/"$backup_file
sudo mv $backup_file /tmp/
backup_file="/tmp/$(basename $backup_file)"
#echo "backup file $backup_file"
sudo chown mathieu:mathieu "${backup_file}"
sudo chmod 777 "${backup_file}"

scp "${backup_file}" mathieu@bk.mathux.org:/srv/share/backup/
sudo rm $backup_file
