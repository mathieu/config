#!/bin/bash

###########################################
#
# Get the daily picture from http://www.bonjourmadame.fr on your desktop
#
# Author : Magetys
# Link : http://www.magetys.com
# Version : 1.0.1
# Dependency : libnotify-bin
# Optionnal : imagemagick
# Note : Works on Gnome and Xfce
#
###########################################
page=

function usage {
    script_name=`basename "$0"`
    echo "Usage $script_name [Day]"
    echo "Day to displae (1 for today, 2 for yesterday, 3 for the day before ...)"
}


function is_a_number {
    if echo $1 | grep '^[0-9]+$' &> /dev/null
    then
        return 0
    else
        return 1
    fi
}

if [ $# != 0 ]
then
    if is_a_number $1 ; 
    then
        usage
        exit
    fi
    page="/page/${1}"
fi

function chkinternet {
    if [ `wget -q -O - google.com | grep -c "<title>Google</title>"` != 0 ]
    then
        echo 1
    else
        echo 0
    fi
}


function fixdbus {
    export DISPLAY=0

    while read line
    do
        echo $line | grep -vqe "^#"
        if [ $? -eq 0 ]; then export $line; fi
    done < ~/.dbus/session-bus/$(cat /var/lib/dbus/machine-id)-$DISPLAY

    echo Dbus fixed OK
}

function resize {
    # Get screen size
    res=
    if [ -e /usr/bin/xrandr ];
    then
        res=$(xrandr | grep '\*' | awk '{print $1}' | head -1)
    else
        res=$(xdpyinfo | grep dimensions | awk '{print $2}')
    fi
    tailleX=$(echo $res | awk -F "x" '{print $1}');
    tailleY=$(echo $res | awk -F "x" '{print $2}');
    echo "Screen Size is X: $tailleX, Y: $tailleY"
    # Get image size
    res=$(identify -format '%w %h\n' $1)
    imgX=$(echo $res | awk '{print $1}')
    imgY=$(echo $res | awk '{print $2}')
    echo "Original Image Size is X: $imgX, Y: $imgY"
    # Calculate resize factor
    mult=0
    div=0
    # If using $tailleX and $imgX as factor scale for Y -> use it! It will perfectly fitt for X and do the best for Y
    if [[ $tailleY -gt $((($tailleX*$imgY)/$imgX)) ]];
    then
        mult=$tailleX
        div=$imgX
    else
        mult=$tailleY
        div=$imgY
    fi
    res=$((($imgX*$mult)/$div))'x'$((($imgY*$mult)/$div))
    echo "New size is $res"
    `mogrify -resize $res $1`;
}

# please install libnotify-bin to popup notification
function notify {
    if [[ -e /usr/bin/notify-send ]]
    then
        notify-send "Bonjour $USER" "Je suis arrivee sur ton bureau" -i $todaywp
    fi
}

dirprefix=$HOME/Images
fileprefix=$dirprefix/bonjourmadame-wp-

todaywp=$fileprefix$(date +%y-%m-%d-%H-%M).jpg

while [ $(chkinternet) != "1" ]; do sleep 15; done
echo Internet connection OK

if [[ ! -d $dirprefix ]]
then
    echo "Creating $dirprefix";
    mkdir $dirprefix;
fi

wget -O - "http://www.bonjourmadame.fr${page}" | grep -Eo "(http://[0-9]+.media.tumblr.com/[0-9a-z]+/tumblr[^\"]+)" | head -n 1 | wget -q -i - -O $todaywp
echo Photo downloaded OK

#ls $fileprefix* | sort -r | tail -n +2 | xargs rm -f
if [[ -e /usr/bin/identify ]]
then
resize $todaywp;
echo Photo Resize OK;
fi

fixdbus

if [[ -e /usr/bin/feh ]]
then
DISPLAY=:0 feh --bg-center $todaywp
fi
if [[ -e /usr/bin/xfconf-query ]]
then
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-path -s $todaywp
fi
if [[ -e /usr/bin/gconftool-2 ]]
then
    gconftool-2 -t str --set /desktop/gnome/background/picture_options "centered" --type string --set /desktop/gnome/background/picture_filename $todaywp
fi

notify

