#!/bin/bash

log_directory=~/log
log_sample="Marvell :\n\nFait (imprévu):\n\t\t\nFait (prévu):\n\t\t\nEn cours (prévu):\n\t\t\nPrévisionnel:\n\t\t\nPrévisionnel (à plus long terme):\n\t\t\n\n\nLundi:\n\nMardi:\n\nMercredi:\n\nJeudi:\n\nVendredi:\n\n\n\n\n"

SPM=60
MPH=60
HPD=24

mode_available="view edit list toMail todo sendMail"

usage () {
    echo "usage : ${0##*/} MODE [DATE|select] "
    echo -e "\twith MODE in : $mode_available"
    echo -e "\tDATE format is YYYY_MM_DD"
    echo ""
}

read_date () {
    if [ -z $1 ]; then
        return 0
    fi
	day=${1#*_*_}
	tmp=${1%_*}
	month=${tmp#*_}
	year=${tmp%_*}

	old=$(date -d "$year-$month-$day" +%s)
	new=$(date +%s)

	diff=$new-$old
	return $((diff / SPM / MPH / HPD))

}

check_valid_argument () {
    needle=$1
    for hay in $2; do
        [[ $hay == $needle ]] && return 1
    done
    return 0
}

create_file () {
    if [ ! -e $1 ]
    then
        mkdir -p $log_directory
        echo -e  -n $log_sample > $1
    fi

}

view () {
    read_date $1
    date_arg=$?
    curr_day=$(date --date="$date_arg days ago" +%u)
    day_to_monday=$(($curr_day -1))
    date_to_edit=$(date  --date="$((date_arg + day_to_monday)) days ago" +%y_%m_%d)

    file=$log_directory/$date_to_edit

    create_file $file
    more $log_directory/$date_to_edit
}

edit () {
    read_date $1
    date_arg=$?
    curr_day=$(date --date="$date_arg days ago" +%u)
    day_to_monday=$(($curr_day -1))
    date_to_edit=$(date  --date="$((date_arg + day_to_monday)) days ago" +%y_%m_%d)

    file=$log_directory/$date_to_edit

    create_file $file
    ${EDITOR} $log_directory/$date_to_edit
}


list () {
	logs=$(ls $log_directory | grep ".*_.*_.*"| sort -r )
    echo "${logs}"
}

toMail () {
    read_date $1
    date_arg=$?
    curr_day=$(date --date="$date_arg days ago" +%u)
    day_to_monday=$(($curr_day -1))
    day_to_friday=$((5 - $curr_day))
    start_date=$(date  --date="$((date_arg + day_to_monday)) days ago" +%Y-%m-%d)
    stop_date=$(date  --date="$((date_arg - day_to_friday)) days ago" +%Y-%m-%d)
    echo "Weekly $start_date -> $stop_date"
    view $1 
}

sendMail () {
    read_date $1
    date_arg=$?
    curr_day=$(date --date="$date_arg days ago" +%u)
    day_to_monday=$(($curr_day -1))
    day_to_friday=$((5 - $curr_day))
    start_date=$(date  --date="$((date_arg + day_to_monday)) days ago" +%Y-%m-%d)
    stop_date=$(date  --date="$((date_arg - day_to_friday)) days ago" +%Y-%m-%d)
	view $1 |mail -s "Weekly $start_date -> $stop_date" $USER@dxo.com 
}

todo () {
    ${EDITOR} $log_directory/TODO
}

check_valid_argument $1 "$mode_available"
valid=$?

if [[ $# -ge 1 && $valid = 1 ]]
then
    date=$2
    if [[ "$2" = "select" ]]
    then
        PS3='Choose Your Date '
        logs=$(ls $log_directory | sort -r)
        select date in $logs quit
        do
            echo
            echo "date $date selected"
            echo
            break
        done
        if [[ "$date" == "quit" ]]
        then
            echo "Exiting"
            exit 0
        fi

    fi
    eval $1 $date
else
    usage
fi

