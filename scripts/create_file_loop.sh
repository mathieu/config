#!/bin/sh

# Create a loop file formated in ext2

txtRED='\e[1;31m' # Red
txtDFL='\e[0m' # Default
scriptName=`echo "$0" | awk -F / '{print $NF}'`

if [ ! $# -eq 2 ]; then
    echo
    echo -e  "${txtRED}USAGE${txtDFL}: $scriptName <FileSize in M> <FileName>"
    echo
    exit 0;
fi

dd if=/dev/zero of=$2 bs=1M count=$1
sudo losetup /dev/loop0 $2
sudo mkfs.ext2 /dev/loop0
sudo losetup -d /dev/loop0

echo
echo -e "${txtRED}Finished!!${txtDFL} File could be mount using mount -o loop $2 /mnt"
echo
exit 1
