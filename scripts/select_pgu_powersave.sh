#!/bin/sh

enableui=1

enable_method () {
	echo "$1" > /sys/class/drm/card0/device/power_method
}

enable_profile () {
	echo "$1" > /sys/class/drm/card0/device/power_profile
}

display_result () {

	if [ -e /sys/kernel/debug/dri/0/radeon_pm_info ]
	then
		res=$(cat /sys/kernel/debug/dri/0/radeon_pm_info)
		echo "$res"
	else
		echo "You can watch GPU frequency by mountint debug fs"
		echo "mount -t debugfs none /sys/kernel/debug"
		echo "cat /sys/kernel/debug/dri/0/radeon_pm_info"
	fi
}


displayui () {
	PS3='Choose Power method: ' # Sets the prompt string.
	# Otherwise it defaults to #? .
	echo

	select methode in "dynpm" "profile"
	do
		echo
		echo "Methode $methode choose"
		echo
		break
	done


	if [ "$methode" = "dynpm" ]
	then
		enable_method $methode
		display_result
		exit;
	fi

	PS3='Choose Power profile '
	select profile in "low" "mid" "high" "auto" "default"
	do
		echo
		echo "Profile $profile choose"
		echo
		break
	done

	enable_method $methode
	enable_profile $profile

}

if [[ $# = 1 && "$1" = "dynpm" ]]
then
	enableui=0;
	enable_method $1;
elif [[ $# = 2 && "$1" = "profile" ]]
then
	case $2 in
		"low" | "mid" | "high" | "auto" | "default" ) enableui=0; enable_method $1; enable_profile $2;;
	*) enableui=1;;
	esac
else
	enableui=1
fi

if [ $enableui ]
then
	displayui
fi

display_result

exit 0


