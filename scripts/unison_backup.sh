unison_exe="/usr/bin/unison"
tmp_file=$(mktemp)

for profile in $@;
do
	res="$(${unison_exe} -batch $profile > ${tmp_file} 2>&1)"
	file_sync=$?
	
	if [ "$file_sync" -ne "0" ];
	then
		echo "return code $file_sync"
		more ${tmp_file}
	fi
done

rm $tmp_file
