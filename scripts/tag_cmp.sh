#!/bin/bash

TAG1=$1
TAG2=$2

if [ $# -eq 0 ]; then
    echo 'Missing parameters'
    exit 0
fi

#git log --decorate  --abbrev-commit --no-merges --format="%C(auto) %h %s" "$TAG1".."$TAG2"
git log --decorate --no-merges  "$TAG1".."$TAG2"

exit 0
