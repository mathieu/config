#!/bin/bash

# Verification de la connection internet
while [ $(chkinternet) != "1" ]; do sleep 15; done
echo Internet connection [OK]

# Declaration des variables et chemins
prefix=$HOME/bonjourmadame/bonjourmadame.jpg
fileprefix=$HOME/bonjourmadame/bonjourmadame_
todaywp=$fileprefix$(date +%d_%m_%y).jpg

# mis à jour du fond d'écran
gconftool -s -t string /desktop/gnome/background/picture_filename $HOME/bonjourmadame/load.jpg
sleep 2
wget -O - http://www.bonjourmadame.fr | grep -Eo "(http://www.bonjourmadame.fr/photo/[^\"]+)|(http://[0-9]+.media.tumblr.com/tumblr[^\"]+)" | head -n 1 | wget -q -i - -O $prefix
sleep 2
gconftool -s -t string /desktop/gnome/background/picture_filename $HOME/bonjourmadame/bonjourmadame.jpg
