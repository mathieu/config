#!/bin/bash

REMOTE=$(git remote)
BRANCH=$(git branch | grep '*' | sed 's/* //')
TAG=$1
MSG=$2

if [ $# -eq 0 ]; then
    echo 'Missing parameters'
    exit 0
fi

git tag -a $TAG -m "$MSG"
git push $REMOTE --tags

exit 0
