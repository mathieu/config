#!/bin/sh
MAILMAN_HOME=/var/lib/mailman
MAIL_PREFIX=noel
export PATH=$MAILMAN_HOME/bin:$PATH
f=`mktemp`
echo max_message_size = 5000 > $f
for l in `list_lists --bare|grep $MAIL_PREFIX` ; do
	config_list -i $f $l
done
rm $f 
