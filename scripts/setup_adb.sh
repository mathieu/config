#!/bin/bash
ADB=$(which adb)

have_device() {
	res=$($ADB devices | grep "0123456789ABCDEF")
	if [ $? != 0 ];
	then
		echo "cannot find Marvell devices"
		exit 0;

	fi
}

#
#	Check adb rights on local
#

user_adb=$(ps aux | grep "adb fork-server server" | grep -v grep| awk '{print $1}')
is_ready=1
if [ "$user_adb" != "root" ];
then
	echo "restart local adb server as root"
	$ADB kill-server
	sudo $ADB devices
fi

have_device

#
#	Check adbd rights on device
#

res=$($ADB shell id | awk '{print $1}' | grep shell)

if [ $? == 0 ];
then
	$ADB root
	sleep 1
fi


#
#	Check filesystem on device
#

have_device
res=$($ADB shell mount | grep "/system" | awk '{print $4}' | grep ro)

if [ $? == 0 ];
then
	$ADB remount
fi

echo "You can access to the board with 'adb shell'"

