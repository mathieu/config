#!/usr/bin/python2
import subprocess
import sys
MAILMAN_HOME = "/usr/lib/mailman/bin"
MAILING_PREFIX = "noel"
ADMIN_MAIL = "mathieu@mathux.org"
ADMIN_PWD = ""


def getListUser(listName):
    cmd = MAILMAN_HOME+"/list_members "+listName
    proc = subprocess.Popen(cmd,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    (stdout, stderr) = proc.communicate()
    ret = proc.returncode
    if ret != 0:
        print "error when checking list %s : %s" % (listName, stderr)
        return []
    return stdout.split('\n')


def getMailingLists(prefix):
    cmd = MAILMAN_HOME+"/list_lists -b"
    proc = subprocess.Popen(cmd,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    (stdout, stderr) = proc.communicate()
    ret = proc.returncode
    if ret != 0:
        print "error when getting mailing lists : %s" % stderr
        sys.exit(1)
    lists = []
    for line in stdout.split('\n'):
        if line.startswith(prefix):
            lists.append(line)
    return lists


def createList(name):
    cmd = MAILMAN_HOME + \
        "/newlist -a -l fr %s %s %s" % (name, ADMIN_MAIL, ADMIN_PWD)
    proc = subprocess.Popen(cmd,
                            shell=True,
                            stdin=sys.stdin,
                            stdout=sys.stdout,
                            stderr=sys.stderr)

    (stdout, stderr) = proc.communicate()
    ret = proc.returncode
    if ret != 0:
        print "error when creating mailing lists : %s" % stderr
        sys.exit(1)


def listContainsUser(user, listName):
    return user in getListUser(listName)


def addUserToList(userEmail, listName):
    if listContainsUser(userEmail, listName):
        return
    print "Add %s to list %s" % (userEmail, listName)
    cmd = MAILMAN_HOME+"/add_members -w n -r - "+listName
    proc = subprocess.Popen(cmd,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    (stdout, stderr) = proc.communicate(input=""+userEmail)
    ret = proc.returncode
    if ret != 0:
        print "error when adding user %s to list %s : %s" % (userEmail, listName, stderr)
        sys.exit(1)


def delUserFromList(userEmail, listName):
    if not listContainsUser(userEmail, listName):
        return
    print "Remove %s from list %s" % (userEmail, listName)
    cmd = MAILMAN_HOME+"/remove_members "+listName+" "+userEmail
    proc = subprocess.Popen(cmd,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    (stdout, stderr) = proc.communicate()
    ret = proc.returncode


# --------------------MAIN--------------------
if len(sys.argv) < 3:
    print "Usage %s nom email [ancien_email]" % sys.argv[0]
    print "nom : nom de la mailing list de l'user sans prefix (ex: celine et pas %s-celine). Sera creee si elle n'exite pas" % MAILING_PREFIX
    print "email : email a ajouter dans toutes les mailing lists sauf dans %s-nom" % MAILING_PREFIX
    print "ancien_email : email a supprimer de toutes les mailing lists"
    sys.exit(1)

mailingName = MAILING_PREFIX+"-"+sys.argv[1]
existingLists = getMailingLists(MAILING_PREFIX)

print "existing lists : %s" % existingLists
if mailingName not in existingLists:
    print "Create list %s " % mailingName
    createList(mailingName)
    # populate new mailing list with existing users
    users = getListUser(MAILING_PREFIX)
    for user in users:
        addUserToList(user, mailingName)

allButUserList = list(existingLists)

if mailingName in existingLists:
    allButUserList.remove(mailingName)

for listName in allButUserList:
    addUserToList(sys.argv[2], listName)

if len(sys.argv) == 4:
    for listName in existingLists:
        delUserFromList(sys.argv[3], listName)
