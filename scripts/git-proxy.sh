#!/bin/bash
# Socat should be installed !
# Then put core.gitProxy=git-proxy.sh in .gitconfig OR
# export GIT_PROXY_COMMAND=git-proxy.sh
# cf. http://blog.tensin.org/index.php?post/2010/12/03/Acc%C3%A9der-%C3%A0-un-repository-GIT-derri%C3%A8re-un-proxy-authentifi%C3%A9.

HTTP_PROXY_HOSTNAME="10.66.243.130"
HTTP_PROXY_PORT="8080"
socat STDIO PROXY:${HTTP_PROXY_HOSTNAME}:$1:$2,proxyport=${HTTP_PROXY_PORT}
