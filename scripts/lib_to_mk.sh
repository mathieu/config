DEPS=""
LIBS=""
for file in $(ls $@);
        do echo -e "
include \$(CLEAR_VARS)
LOCAL_MODULE := ${file%.so}
LOCAL_SRC_FILES := vendor/lib/$file
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := \$(TARGET_OUT)/vendor/lib
include \$(BUILD_PREBUILT)";
        DEPS+=" ${file%.so}"
        LIBS+=" $file"
        done
echo -e "\n$DEPS"
echo -e "\n$LIBS"
