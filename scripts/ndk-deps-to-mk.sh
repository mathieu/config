#!/bin/bash

# To be used in the root path of system partition dump
# Will only get lib in vendor/lib

LIBS=""
DEPS=""
APP="${1#bin\/}"
while read line; do
	lib=$(echo $line | awk -F" -> " '{print $1}')
	path=$(echo $line | awk -F" -> " '{print $2}')
		LIBS+="$path "
		DEPS+="${lib%.so} "
		echo -e "
include \$(CLEAR_VARS)
LOCAL_MODULE := ${lib%.so}
LOCAL_SRC_FILES := $path
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := \$(TARGET_OUT)/${path%${lib}}
include \$(BUILD_PREBUILT)"
done  < <(ndk-depends -Lvendor/lib --print-paths $@ | grep -v "Could not" | grep -v "\\$" | grep vendor)

echo -e "
include \$(CLEAR_VARS)
LOCAL_MODULE := $APP
LOCAL_SRC_FILES := system/bin/$APP
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_PATH := \$(TARGET_OUT)/bin
LOCAL_MODULE_SUFFIX := 
LOCAL_SHARED_LIBRARIES := $DEPS
include \$(BUILD_PREBUILT)
"

echo "Install your lib using"
echo "cp $LIBS YOUR_INSTALL_PATH"
