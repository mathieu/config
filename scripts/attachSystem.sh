#!/bin/bash

if [[ $# -eq 0 ]]; then
    adb wait-for-device && adb forward tcp:5039 tcp:5039
    pid=$(adb shell ps | grep system_server | awk '{print $2}')
    adb shell gdbserver :5039 --attach $pid
else
    adb wait-for-device && adb forward tcp:5040 tcp:5040 && adb shell gdbserver :5040 --attach $( adb shell ps | grep $@ | awk '{print $2}')
fi

