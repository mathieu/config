"""""""""""""""
" SHORTCUTS "
"""""""""""""
"   F1          help
"   S-F1        open vimrc
"   M-S-F1      vim help
"   C-F1        LSP Menu
"   F2          open file in a new tab
" C-F2          open FZF in a new tab
" S-F2          Split and open file
"   F3          autotags Update
" S-F3          autotags Add
"   F4          open include file
"   F5          find C symbol
"   F6          go to definition
"   F7          go to calls
"   F8          view tag list
" S-F8          Open DB building menu
" M-F8          build kernel ctags/cscope databases
"   F9          Open NerdTree. s to open in split. t to open in tab
" M-F9          Show diff line
" S-F9          Highlight diff line
"   F10         folding
" S-F10         unfolding
"   F11         unhighlight search
"   F12         paste mode
" C-left/right  switch tab
" C-up/down     switch window
" M-left/right  horizontal size
" M-up/down     vertical size
" gc            comment/uncomment
" C-K in normal:indent selection
" \-a           Set Kernel Coding style
" from a.vim
" :A[SVTN]      Switch between header/file , split, vertical, tab, next match
" :IH[SVTN]     Switch to file under cursor
"\o to run FZF
" Use snippets with tab e.g : for <tab> (see .vim/bundle/vim-snippets/snippets for available
" snippets)
" look at :help index
" or
" :verbose map <key>
" or for all keys
":redir! > vim_maps.txt
":map " Normal, visual, select and operator bindings
":map! "insert and command-line mode bindings
":redir END
" C-K in insert for ligature. See available ligature with :digraphs
" Could be disabled with :set conceallevel=0. See after/syntax for custom
" settings
" Require a font with ligature support

"""""""""""
" GENERAL "
"""""""""""
" move around where there is no char, insert space when inserting
set virtualedit=all
" disable vi-compatible mode
set nocompatible

" Switch syntax highlighting on when the terminal has colors or when using the
" GUI (which always has colors).
if &t_Co > 2 || has("gui_running")
        " Revert with ":syntax off".
        syntax on

        " I like highlighting strings inside C comments.
        " Revert with ":unlet c_comment_strings".
        let c_comment_strings=1
endif

filetype plugin on
" enable file type detection and do language-dependent indenting
filetype plugin indent on
"set clipboard=unnamed
" Add to .vimrc to enable project-specific vimrc

"set exrc
"set secure

" exrc allows loading local executing local rc files.
" secure disallows the use of :autocmd, shell and write commands in local .vimrc files.

" save undo trees in files
set undofile
if has('nvim')
set undodir=~/.config/nvim/undo
else
set undodir=~/.vim/undo
end
" number of undo saved
set undolevels=10000

""""""""
" SAVE "
""""""""
" disable backup
set nobackup
" save before compilation
set autowrite
" Put these in an autocmd group, so that you can revert them with:
" ":augroup vimStartup | au! | augroup END"
augroup vimStartup
        au!
        " When editing a file, always jump to the last known cursor position.
        " Don't do it when the position is invalid or when inside an event handler
        " (happens when dropping a file on gvim).
        autocmd BufReadPost *
                                \ if line("'\"") >= 1 && line("'\"") <= line("$") |
                                \   exe "normal! g`\"" |
                                \ endif

        " Check that file have not been changed
        " CursorHold : after cursor move
        " WinEnter or BufWinEnter
        au WinEnter * checktime
        au BufWinEnter * checktime
augroup END

" command line history
set history=1000
set tabpagemax=50

"""""""""
" INPUT "
"""""""""
" allow backspacing in insert mode
set backspace=indent,eol,start
" don't use Ex mode, use Q for formatting
map Q gq
" Enable mouse for 'a'll mode
set mouse=a
" key combination timeout
set notimeout
"set timeoutlen=4000
" set timeout for multicharacter keys codes (like <F1>)
set ttimeout
" set timeout to tenth of a second (could be increased for slow terminals)
set ttimeoutlen=100
" key event timeout
set updatetime=1000

" Use hjkl-movement between rows when soft wrapping.
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" move visual block around
vnoremap <S-Down> :m '>+1<cr>gv=gv
vnoremap <S-Up> :m '<-2<cr>gv=gv
vnoremap <S-Right> >gv
vnoremap <S-Left> <gv
"""""""""""
" DISPLAY "
"""""""""""
" highlight the cursor line
set cursorline
" show the cursor position
set ruler
" show mode
set showmode
" display incomplete commands
set showcmd
" display line number"
set number
" set status line
set statusline=%<%f\ %h%w%m%r%3.(\ %)%{fugitive#statusline()}%=%([%{Tlist_Get_Tagname_By_Line()}]%)%3.(\ %)%-14.(%l,%c%V%)\ %P
" always display status line
set laststatus=2
set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line

" show bad white spaces
let c_space_errors = 1
let python_space_error_highlight = 1
highlight link cSpaceError SpaceError
highlight link pythonSpaceError SpaceError
highlight SpaceError ctermfg=235 cterm=reverse

"""""""""""""""
" INDENTATION "
"""""""""""""""
" use tabs at the start of a line, spaces elsewhere
set smarttab
set smartindent
set autoindent
"" tab=4
"set tabstop=4
"set softtabstop=4
"set shiftwidth=4
" tab=8
set tabstop=8
set softtabstop=8
set shiftwidth=8
" real tabs
" set noexpandtab

nnoremap <silent> <leader>gg :tab term ++close lazygit<CR>

"filetype specific action
augroup FtSpecific
        au Filetype markdown set tabstop=4 shiftwidth=4 softtabstop=4
        au BufRead,BufNewFile *.iris set ft=python
        au BufRead,BufNewFile *.ino set tabstop=4 shiftwidth=4 softtabstop=4
        au BufRead,BufNewFile *.asm set ft=nasm
        "hand written GNU AS
        au BufRead,BufNewFile *.S set ft=asm
        au BufRead,BufNewFile *.logcat set filetype=logcat
        au BufRead,BufNewFile logcat set filetype=logcat
        au BufNewFile,BufRead *.lc set filetype=logcat

        " python
        " autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab
        autocmd Filetype python set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
        autocmd Filetype python set tabstop=4 shiftwidth=4 softtabstop=4
        autocmd Filetype python set expandtab
        autocmd Filetype python set modeline
        " Indentation with = for python need autopep8
        autocmd Filetype python set equalprg=autopep8\ -
        iab sefl self
        iab slef self
augroup END

" vim -b : edit binary using xxd-format!
augroup Binary
        au!
        au BufReadPre  *.bin let &bin=1
        au BufReadPost *.bin if &bin | %!xxd
        au BufReadPost *.bin set ft=xxd | endif
        au BufWritePre *.bin if &bin | %!xxd -r
        au BufWritePre *.bin endif
        au BufWritePost *.bin if &bin | %!xxd
        au BufWritePost *.bin set nomod | endif
augroup END


fu DetectIndent()
        if len(expand('%')) == 0
                return
        endif

        if filereadable(".clang-format")
                return
        endif
        execute system ('python3 ~/.vim/indent_finder/indent_finder.py --vim-output --default space --default-size ' . &tabstop .' "' . expand('%') . '"' )
        if &expandtab
                let b:clang_style="{BasedOnStyle: LLVM, AlignConsecutiveAssignments: true, AllowShortFunctionsOnASingleLine: Empty, BreakBeforeBraces: Linux, BreakStringLiterals: false, ColumnLimit: 120, IndentCaseLabels: true, IndentWidth: " .&shiftwidth .", TabWidth: " .&tabstop .", UseTab: Never}"
        else
                let b:clang_style="{BasedOnStyle: LLVM, AlignConsecutiveAssignments: true, AllowShortFunctionsOnASingleLine: Empty, BreakBeforeBraces: Linux, BreakStringLiterals: false, ColumnLimit: 120, IndentCaseLabels: true, IndentWidth: " .&shiftwidth .", TabWidth: " .&tabstop .", UseTab: ForContinuationAndIndentation}"
        endif
endfu

" detect indentation see http://www.freehackers.org/Indent_Finder
augroup IndentFind
        autocmd BufReadPost /* call DetectIndent()
augroup END

noremap <C-K> :py3f ~/.vim/syntax/clang-format.py<CR>
"inoremap <C-K> <ESC>:py3f ~/.vim/syntax/clang-format.py<CR>i
noremap <leader>cr :py3f ~/.vim/syntax/clang-rename.py<cr>

set wildignore+=*.o,*.d,*.dex,*.class,*.png,*.jpeg,*.jpg,*.pdf

""""""""""
" SEARCH "
""""""""""

" highlight search
set hlsearch
" unhighlight current search
map <silent> <F11> :nohlsearch<CR>
imap <F11> <ESC><F11>a
" highlight search while typing
set incsearch
" show matching brackets -> key %
set showmatch
" tenths of a second before blink matching brackets
set mat=5
" search word and list lines to jump with F3
" map <F3> [I:let nr = input("Which one: ") <Bar>execute "normal " . nr ."[\t"<CR>
" go to declaration with F5
" map <silent> <F5> gd:nohlsearch<CR>
" imap <F5> <ESC><F5>i

" plugin taglist
let Tlist_Ctags_Cmd = '/usr/bin/ctags'
let Tlist_Process_File_Always = 1
let Tlist_Exit_OnlyWindow = 1
"let Tlist_Close_On_Select = 1
let Tlist_Auto_Highlight_Tag = 1
let Tlist_Display_Prototype = 0
let Tlist_Display_Tag_Scope = 0
let Tlist_Show_One_File = 1
let Tlist_Compact_Format = 1
let Tlist_Enable_Fold_Column = 0
" sort by name or order ?
let Tlist_Sort_Type = "name"
"let Tlist_File_Fold_Auto_Close = 1
let Tlist_Inc_Winwidth = 0
"let Tlist_Use_Horiz_Window = 1
let Tlist_Use_Right_Window = 1
" open/close tag list window with F8
"map <silent> <F8> :TlistToggle<CR>
nmap <F8> :TagbarToggle<CR>
"map <silent> <F8> :Vista!!<CR>


" close preview window after a completion
augroup AutoCompletion
        autocmd CursorMovedI *.{[hc],cpp} if pumvisible() == 0|pclose|endif
        autocmd InsertLeave *.{[hc],cpp} if pumvisible() == 0|pclose|endif
augroup END

function! TagInNewTab()
        let word = expand("<cword>")
        redir => tagsfiles
        silent execute 'set tags'
        redir END
        tabe
        execute 'setlocal' . strpart(tagsfiles, 2)
        execute 'tag ' . word
endfunction

map    <S-T>    :call TagInNewTab()<CR>

"""""""""
" PASTE "
"""""""""
" set/unset paste mode with F12/shift+F12
map <F12> :set paste<CR>
map <S-F12> :set nopaste<CR>
imap <F12> <C-O>:set paste<CR>
imap <S-F12> <nop>
set pastetoggle=<S-F12>
" paste with reindent with Esc prefix
"nnoremap <Esc>P P'[v']=
"nnoremap <Esc>p p'[v']=

""""""""""
" WINDOW "
""""""""""
" create window below or at right of the current one
set splitbelow
set splitright
" if multiple windows
if bufwinnr(1)
        if $TERM == "screen"
                nnoremap <M-Up>    :TmuxNavigateUp<cr>
                nnoremap <M-Right> :TmuxNavigateRight<cr>
                imap <M-Up>    <ESC>:TmuxNavigateUp<cr>
                imap <M-Right> <ESC>:TmuxNavigateRight<cr>
                nnoremap <M-Left>  :TmuxNavigateLeft<cr>
                nnoremap <M-Down>  :TmuxNavigateDown<cr>
                imap <M-Left>  <ESC>:TmuxNavigateLeft<cr>
                imap <M-Down>  <ESC>:TmuxNavigateDown<cr>
        endif
        " switch to next/previous tab with ctrl+right/ctrl+left
        map <C-Right> gt
        map <C-Left> gT
        imap <C-Right> <ESC><C-Right>a
        imap <C-Left> <ESC><C-Left>a
        " switch to next/previous window with ctrl+down/ctrl+up
        map <C-Down> <C-W>w
        map <C-Up> <C-W>W
        imap <C-Down> <ESC><C-Down>a
        imap <C-Up> <ESC><C-Up>a
endif
" open automatically quickfix window
augroup QuickFix
        autocmd QuickFixCmdPost * cw
augroup END

" open a file in the same directory as the current file with F2 and split with shift+F2
map <F2> :tabe <C-R>=expand("%:h") . "/"<CR>
nmap <S-F2> :split <C-R>=expand("%:h") . "/"<CR>
map <C-F2> :call FZFTab()<CR>
fun! FZFTab()
        tabe
        FZF
endfun

""""""""
" HELP "
""""""""
" allow embedded man page
runtime! ftplugin/man.vim
" show vimrc with shift+F1
nnoremap <silent> <S-F1> :tabe $MYVIMRC<CR>gg
inoremap <S-F1> <Esc><S-F1>
" show contextual help with F1

" get doc of std::string instead of string
" Need installation of cppMan
function! s:JbzCppMan()
    let old_isk = &iskeyword
    setl iskeyword+=:
    let str = expand("<cword>")
    let &l:iskeyword = old_isk
    execute 'Man ' . str
endfunction

function Help()
        try
                if exists('b:current_syntax') && b:current_syntax == "python"
                        :call ShowPyDoc(expand("<cword>"), 1)
                elseif exists('b:current_syntax') && b:current_syntax == "cpp"
                        :call s:JbzCppMan()
                elseif has_key(g:LanguageClient_serverCommands, &filetype)
                        :call LanguageClient#textDocument_hover()
                else
                        execute "Man " . expand("<cword>")
                endif
        catch /:E149:/
                execute "help " . expand("<cword>")
        endtry
endfunction
nmap <silent> <F1> :call Help()<CR>
imap <F1> <Esc><F1>
" show VIM help with alt+shift+F1
nmap <silent> <M-S-F1> :help <C-R>=expand("<cword>")<CR><CR>
imap <M-S-F1> <Esc><M-S-F1>

"""""""""
" TOOLS "
"""""""""

" zf#j creates a fold from the cursor down # lines.
"zf/string creates a fold from the cursor to string .
"zj moves the cursor to the next fold.
"zk moves the cursor to the previous fold.
"zo opens a fold at the cursor.
"zO opens all folds at the cursor.
"zm increases the foldlevel by one.
"zM closes all open folds.
"zr decreases the foldlevel by one.
"zR decreases the foldlevel to zero -- all folds will be open.
"zd deletes the fold at the cursor.
"zE deletes all folds.
"[z move to start of open fold.
"]z move to end of open fold.

" fold form this bracket
nmap <F10> zfa{<CR><CR>
" unfold or use End
nmap <S-F10> zo<CR><CR>
" Autosave folding
"au BufWinLeave * mkview
" Autoload folding
"au BufWinEnter * silent loadview


""""""""""""""""""""""""""""""""""""""""""""""""""
"Omni-completion par CTRL-X_CTRL-O
"""""""""""""""""""""""""""""""""""""""""""""""""""
set omnifunc=syntaxcomplete#Complete

" Enable omnicppcompletion
let OmniCpp_ShowAccess = 0
let OmniCpp_LocalSearchDecl=1
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
set path+=..
"**,/usr/local/include,/usr/include

"""""""""""""
" Latex		"
"""""""""""""
" package vim-latexsuite
" Insert the following line in your ~/.vim/ftplugin/tex.vim file:
" imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
" compile with \ll
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "pdflatex"
noremap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
let g:Tex_DefaultTargetFormat="pdf"

if has('gui_running')
        colorscheme darkblue
else
        colorscheme mycolor
endif
cnoremap sudow w !sudo tee % >/dev/null
" Allow saving of files as sudo when I forgot to start vim using sudo.
"cmap w!! w !sudo tee > /dev/null %
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!
"""""""""""
" NerdTree
"""""""""""
augroup NerdGroup
        " Exit Vim if NERDTree is the only window remaining in the only tab.
        autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
        " Close the tab if NERDTree is the only window remaining in it.
        autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
        map <F9>   :NERDTreeToggle<CR>
        let NERDTreeIgnore= ['\.o$', '\.d$']
augroup END

""""""""""
" LocalConfig
""""""""""

if filereadable(expand('~/.vimrc.local'))
        " Plugins list and settings should be loaded
        " only once. Load local_settings block
        let g:local_plugins = 0
        let g:local_settings = 1
        source ~/.vimrc.local
        " Look for .vimrc in working dir
        " Restrict cmdline as this could be seen as security issue
        set exrc
        set secure
endif

""""""""""
" W3m
""""""""""
let g:w3m#external_browser = 'firefox'

""""""""""
" Kernel
""""""""""
let g:linuxsty_patterns = [ "/usr/src/", "/linux" ]
nnoremap <silent> <leader>a :LinuxCodingStyle<cr>

""""""""""
" Debug
""""""""""

"Or start vim with -V[VERBOSENUMBER][FILE]
function! ToggleVerbose()
        if !&verbose
                set verbosefile=~/.log/vim/verbose.log
                set verbose=15
        else
                set verbose=0
                set verbosefile=
        endif
endfunction

""""""""
" Spell
""""""""
" do :set spelllang+=XX to install language
set spelllang=fr,en
" Ctrl-L correct last error
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

"set list
"set listchars=eol:⏎,tab:▸·,trail:␠,nbsp:⎵

""""""""
" GPG
""""""""

augroup encrypted
  au!
  autocmd BufReadPre,FileReadPre *.gpg set viminfo=
  autocmd BufReadPre,FileReadPre *.gpg set noswapfile noundofile nobackup
  autocmd BufReadPre,FileReadPre *.gpg set bin
  autocmd BufReadPre,FileReadPre *.gpg let ch_save = &ch|set ch=2
  autocmd BufReadPost,FileReadPost *.gpg '[,']!gpg --decrypt 2> /dev/null
  autocmd BufReadPost,FileReadPost *.gpg set nobin
  autocmd BufReadPost,FileReadPost *.gpg let &ch = ch_save|unlet ch_save
  autocmd BufReadPost,FileReadPost *.gpg execute ":doautocmd BufReadPost " . expand("%:r")
  autocmd BufWritePre,FileWritePre *.gpg '[,']!gpg --default-recipient-self -ae 2>/dev/null
  autocmd BufWritePost,FileWritePost *.gpg u
augroup END

"""""""""
" Plugin"
"""""""""
" for autotags plugin
let g:autotags_pycscope_cmd = '/usr/local/bin/pycscope'
if !has('nvim')
"Using vundle
"cf. https://github.com/gmarik/Vundle.vim
":PluginInstall to install them
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"Rust
Plugin 'rust-lang/rust.vim'

" Communication with git
Plugin 'tpope/vim-fugitive'
" Support for GBrowse with github
Plugin 'tpope/vim-rhubarb'
" Comment block of code
Plugin 'tpope/vim-commentary'
" Make terminal vim and tmux work better together for Focus.
Plugin 'tmux-plugins/vim-tmux-focus-events'
" language pack
Plugin 'sheerun/vim-polyglot'
" color even for terminal without gui... 
" Plugin 'vim-scripts/CSApprox'
" Show current modified line
" Plugin 'airblade/vim-gitgutter'
" Same as gitgutter but for other cvs
"Plugin mhinz/vim-signify
if has('nvim') || has('patch-8.0.902')
  Plugin 'mhinz/vim-signify'
else
  Plugin 'mhinz/vim-signify', { 'branch': 'legacy' }
endif

" echofunc -> what is the current function
Plugin 'mbbill/echofunc'

" What to test some color ?
" Plugin 'vim-scripts/Colour-Sampler-Pack.git'

" More Snippets :
Plugin 'tomtom/tlib_vim'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'honza/vim-snippets'
Plugin 'garbas/vim-snipmate'
let g:snipMate = { 'snippet_version' : 1 }
Plugin 'sirver/ultisnips'
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsListSnippets = '<s-tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

" Filesystem exploration
Plugin 'preservim/nerdtree'

" Python completion
"Plugin 'klen/python-mode'
"" Python Bundle
"Plugin  'davidhalter/jedi-vim'
"Plugin  'scrooloose/syntastic'
"Plugin  'majutsushi/tagbar'
"Plugin  'Yggdroot/indentLine'
Plugin 'fs111/pydoc.vim'

" Omni completion for cpp
Plugin 'vim-scripts/OmniCppComplete' " Can be forced with set omnifunc=omni#cpp#complete#Main

" List current file function
" let $GIT_SSL_NO_VERIFY = 'true'
Plugin 'gitlab@gitlab.mathux.org:Mathieu/taglist.git'
" Tagbar look like a maintened taglist
Plugin 'majutsushi/tagbar'

" Fuzzy finder
Plugin 'junegunn/fzf' " then run, cd .vim/bundle/fzf && ./install --bin
Plugin 'junegunn/fzf.vim'
nnoremap <silent> <leader>o :FZF<CR>
nnoremap <silent> <leader>O :FZF!<CR>

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-l> <plug>(fzf-complete-line)

if v:version >= 800
        " Async lint
        " Plugin 'dense-analysis/ale'
        let g:ale_fixers = {
                                \   '*': ['remove_trailing_lines', 'trim_whitespace'],
                                \}
        let g:ale_linters = {
                                \  'cpp': ['clangd'],
                                \}

        Plugin 'autozimu/LanguageClient-neovim', {
                                \ 'oninstall': 'bash install.sh',
                                \ }
        let g:LanguageClient_serverCommands = {
                                \ 'cpp': ['clangd', '-background-index',],
                                \ 'c': ['clangd', '-background-index',],
                                \ 'python': ['/usr/bin/pyls'],
                                \ 'rust': ['rustup', 'run', 'stable', 'rls'],
                                \ }
        let g:LanguageClient_loggingFile =  expand('~/.LanguageClient.log')
        " Could be debbuged with
        ":call LanguageClient_setLoggingLevel('DEBUG')
        ":LanguageClientStart

        "Language Client recommanded settings
        Plugin 'Shougo/deoplete.nvim' " Need to install pynvim
        Plugin 'roxma/nvim-yarp'
        Plugin 'roxma/vim-hug-neovim-rpc'
        let g:deoplete#enable_at_startup = 1

        Plugin 'Shougo/echodoc.vim'

        let cmdheight=2
        let g:echodoc#enable_at_startup = 1
        let g:echodoc#type = 'signature'

        function LC_maps()
                if has_key(g:LanguageClient_serverCommands, &filetype)
                        nmap <buffer> <silent> K <Plug>(lcn-hover)
                        nmap <buffer> <silent> gd <Plug>(lcn-definition)
                        nmap <buffer> <silent> gy <Plug>(lcn-type-definition)
                        nmap <buffer> <silent> gi <Plug>(lcn-implementation)
                        nmap <buffer> <silent> gr <Plug>(lcn-references)
                        nmap <buffer> <silent> cr <Plug>(lcn-rename)
                        nmap <buffer> <silent> gq <Plug>(lcn-code-action)
                        nmap <C-F1> <Plug>(lcn-menu)
                endif
        endfunction
        augroup LSP
                autocmd FileType c,cpp,python call LC_maps()
        augroup END


else
        "Syntax checking | Install flake8 or pylint for python
        " Install shellcheck for bash
        Plugin 'vim-syntastic/syntastic'
                let g:syntastic_c_checkers = ['gcc', 'cppcheck']
                "let g:syntastic_cpp_compiler = 'clang++'
                "let g:syntastic_cpp_compiler_options = '-std=c++14'
endif

"Completion (need more configuration for python, c# ...)
"Plugin 'ycm-core/YouCompleteMe'
" VimWiki
Plugin 'vimwiki/vimwiki'
let g:vimwiki_list=[{'path':'~/.vim/vimwiki'}]
Plugin 'mattn/calendar-vim'
" Use \w\w to create new diary entry.
" use :Diary to view all on them
command! Diary VimwikiDiaryIndex
augroup vimwikigroup
    autocmd!
    " automatically update links on read diary
    autocmd BufRead,BufNewFile diary.wiki VimwikiDiaryGenerateLinks
augroup end

" open file at the given line with the file.txt:20 syntax
" But it's screw up the git commit message
"Plugin 'bogado/file-line'

" % match if/then/else/html ...
Plugin 'tmhedberg/matchit'

" Kernel Formatting
Plugin 'gregkh/kernel-coding-style.git'

" Web browsing
"Plugin 'yuratomo/w3m.vim'

" Recognize Key in screen/tmux
"Plugin 'drmikehenry/vim-fixkey'
set term=xterm-256color

Plugin 'tpope/vim-surround'
	" Maps ss to surround word. e.g. ss] to add [] around word
	nmap ss ysiw
	" Maps sl to surround line
	nmap sl yss
	" Surround Visual selection
	vmap s S

"Grammar Checking
Plugin 'rhysd/vim-grammarous'

Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'
augroup LimeGroup
        autocmd! User GoyoEnter Limelight
        autocmd! User GoyoLeave Limelight!
augroup END

"Complete delimiters
Plugin 'Raimondi/delimitMate.git'

Plugin 'Yggdroot/LeaderF'

"Vim session
Plugin 'tpope/vim-obsession'
Plugin 'mhinz/vim-startify'
let g:startify_commands = [
                        \ ['Plugin Update', 'PluginUpdate'],
                        \ ['Plugin Install', 'PluginInstall'],
                        \ ['Vim Wiki', 'VimwikiIndex'],
                        \]
let g:startify_custom_header = []

Plugin 'vim-pandoc/vim-pandoc'
let g:pandoc#modules#disabled = ["folding"]
Plugin 'vim-pandoc/vim-pandoc-syntax'

"Unicode research
"Plugin 'chrisbra/unicode.vim'

" Tig explorer
Plugin 'iberianpig/tig-explorer.vim'

" LaTex
Plugin 'lervag/vimtex'
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'
" not compatible with LaTeX-Box included in polyglot
let g:polyglot_disabled = ['latex']

" Visual incrementation. increment in block with ctrl-A
"-> use g ctr-a instead
"Plugin 'triglav/vim-visual-increment'
"set nrformats=alpha,octal,hex

Plugin 'christoomey/vim-tmux-navigator'
let g:tmux_navigator_no_mappings=1

" view register content
Plugin 'junegunn/vim-peekaboo'

Plugin 'patashish704/pandoc-complete'

" Debugger
Plugin 'puremourning/vimspector'
"let g:vimspector_enable_mappings = 'HUMAN'

" Open browser related things
Plugin 'tyru/open-browser.vim'
let g:openbrowser_search_engines = extend(
                        \ get(g:, 'openbrowser_search_engines', {}),
                        \ {
                        \   'cppreference': 'https://en.cppreference.com/mwiki/index.php?title=Special%3ASearch&search={query}',
                        \   'qt': 'https://doc.qt.io/qt-5/search-results.html?q={query}',
                        \   'devdoc': 'https://devdocs.io/#q={query}',
                        \   'github-cpp': 'http://github.com/search?l=C%2B%2B&q=fork%3Afalse+language%3AC%2B%2B+{query}&type=Code',
                        \   'en2fr': 'http://translate.google.fr/translate_t?hl=fr&ie=UTF-8&text={query}&sl=en&tl=fr#',
                        \   'fr2en': 'http://translate.google.fr/translate_t?hl=fr&ie=UTF-8&text={query}&sl=fr&tl=en#',
                        \   'verbe' : 'http://www.la-conjugaison.fr/du/verbe/{query}.php'
                        \ },
                        \ 'keep'
                        \)
nnoremap <silent> <leader>osx :call openbrowser#smart_search(expand('<cword>'), "cppreference")<CR>
nnoremap <silent> <leader>osq :call openbrowser#smart_search(expand('<cword>'), "qt")<CR>
nnoremap <silent> <leader>osd :call openbrowser#smart_search(expand('<cword>'), "devdoc")<CR>
nnoremap <silent> <leader>osen2fr :call openbrowser#smart_search(expand('<cword>'), "en2fr")<CR>
nnoremap <silent> <leader>osfr2en :call openbrowser#smart_search(expand('<cword>'), "fr2en")<CR>

" A recent tagbar
Plugin 'liuchengxu/vista.vim'
" highlight other use of he word under the cursor
Plugin 'RRethy/vim-illuminate'
" Parentheses with various colors
Plugin 'luochen1990/rainbow'
let g:rainbow_active = 1 "set to 0 if you want to enable it later via :RainbowToggle

" Markdown preview -> then install with :call mkdp#util#install()
Plugin 'iamcco/markdown-preview.nvim'

" File skeleton
Plugin 'pgilad/vim-skeletons'
let skeletons#autoRegister = 1
let skeletons#skeletonsDir = ["~/.vim/templates/"]
"let skeletons#skeletonsDir += ["~/.vim/bundle/vim-skeletons/skeletons/"]

Plugin 'lfilho/cosco.vim'
autocmd FileType c,cpp nmap <silent> <Leader>; <Plug>(cosco-commaOrSemiColon)
autocmd FileType c,cpp imap <silent> <Leader>; <c-o><Plug>(cosco-commaOrSemiColon)
Plugin 'mmarchini/bpftrace.vim'
autocmd BufNewFile,BufRead *.bt setfiletype bpftrace

" Source the termdebug plugin
packadd! termdebug
let g:termdebug_wide=1
" Boormark // visual mark
Plugin 'MattesGroeger/vim-bookmarks'

" open file like filename.ext:NUMBER
Plugin 'bogado/file-line'
call vundle#end()
endif "has(nvim)
