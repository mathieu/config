"""""""""""
" DISPLAY "
"""""""""""
" color palette
"set t_Co=256
" syntax highlighting when the terminal has colors
"
" You can display all the possible usage with ":hi"

if &t_Co > 2 || has("gui_running")
	syntax on
	" my colors
	set background=dark
	highlight clear
	if exists("syntax_on")
		syntax reset
	endif
if &t_Co == 256
	highlight! Normal         ctermfg=77          ctermbg=Black
	highlight! Statement      ctermfg=254         ctermbg=Black
	highlight! Comment        ctermfg=191         ctermbg=Black
	highlight! PreProc        ctermfg=200         ctermbg=Black
else
	highlight! Normal         ctermfg=Green       ctermbg=Black
	highlight! Statement      ctermfg=White       ctermbg=Black
	highlight! Comment        ctermfg=Yellow      ctermbg=Black
	highlight! PreProc        ctermfg=Magenta     ctermbg=Black
endif
	highlight! Constant       ctermfg=Red         ctermbg=Black
	highlight! def link Structure Statement
	highlight! def link StorageClass Statement
	highlight! Type           ctermfg=Grey        ctermbg=Black
	highlight! def link Identifier Type
	highlight! Todo           ctermfg=Black       ctermbg=DarkRed
	highlight! Search         ctermfg=Black       ctermbg=Yellow
	highlight! def link IncSearch Visual
if &t_Co == 256
	highlight! DiffAdd                            ctermbg=54          cterm=none
	highlight! DiffDelete     ctermfg=Black       ctermbg=54          cterm=none
else
	highlight! DiffAdd                            ctermbg=DarkMagenta cterm=none
	highlight! DiffDelete     ctermfg=Black       ctermbg=DarkMagenta cterm=none
endif
	highlight! def link DiffChange DiffAdd
	highlight! DiffText       ctermfg=White       ctermbg=DarkBlue    cterm=none
	highlight! def link Folded TabLineSel
	highlight! def link FoldColumn Folded
	highlight! MatchParen                         ctermbg=Blue
if &t_Co == 256
	highlight! BadWhitespace                      ctermbg=235
elseif &t_Co > 8
	highlight! BadWhitespace                      ctermbg=DarkGrey
else
	highlight! BadWhitespace                      ctermbg=DarkCyan
endif
	highlight! Pmenu          ctermfg=Grey        ctermbg=DarkBlue
if &t_Co > 8
	highlight! PmenuSel       ctermfg=Yellow      ctermbg=Blue
else
	highlight! PmenuSel       ctermfg=Yellow      ctermbg=Cyan
endif
	highlight! def link PmenuSbar Pmenu
	highlight! PmenuThumb                         ctermbg=DarkMagenta cterm=none
if &t_Co == 256
	highlight! LineNr         ctermfg=DarkCyan    ctermbg=235
	highlight! CursorLine                         ctermbg=236         cterm=none
else
	highlight! LineNr         ctermfg=DarkCyan    ctermbg=Black
	highlight! CursorLine                         ctermbg=DarkGrey    cterm=none
endif
	highlight! def link CursorColumn CursorLine
	highlight! Visual         ctermfg=White       ctermbg=Blue        cterm=none
	highlight! VertSplit      ctermfg=Grey        ctermbg=Grey        cterm=none
	highlight! StatusLine     ctermfg=Black       ctermbg=Grey        cterm=none
if &t_Co > 8
	highlight! StatusLineNC   ctermfg=DarkGrey    ctermbg=Grey        cterm=none
else
	highlight! StatusLineNC   ctermfg=Cyan        ctermbg=Grey        cterm=none
endif
	highlight! def link TabLine StatusLineNC
	highlight! def link TabLineFill TabLine
if &t_Co == 256
	highlight! TabLineSel     ctermfg=White       ctermbg=235         cterm=none
elseif &t_Co > 8
	highlight! TabLineSel     ctermfg=White       ctermbg=Black       cterm=none
else
	highlight! TabLineSel     ctermfg=White       ctermbg=Black
endif
	highlight! ModeMsg        ctermfg=DarkCyan    ctermbg=Black
	highlight! WarningMsg     ctermfg=Yellow      ctermbg=Black
	highlight! ErrorMsg       ctermfg=Red         ctermbg=Black
endif


hi SpellBad ctermbg=None ctermfg=Red cterm=underline
hi SpellCap ctermfg=12 ctermfg=White gui=undercurl guisp=Blue
" to be used with set colorcolumn=81
highlight ColorColumn ctermbg=236
highlight ALEError ctermbg=DarkMagenta cterm=bold
highlight ALEWarning ctermbg=DarkMagenta cterm=bold
" define whitespaces at end of line as bad whitespaces
match BadWhitespace /\s\+$/
" define extra spaces at the front of the line as bad whitespaces
match BadWhitespace /^\ \+/
" define redundant whitespaces as bad whitespaces
match BadWhitespace /\s\+$\| \+\ze\t/
