== generate report ==
find -name "*.c" -o -name "*.cpp" | grep -v lib | xargs cppcheck 2> repport

* Error list cppcheck --errorlist | grep -v "severity=\"style\"" | sed "s/.*msg=\"\(.*\)\".*/\1/g" | sed '/^\s*$/d' | recode xml..ascii

== more tools ==
* git pre-commit https://github.com/danmar/cppcheck/blob/master/tools/git-pre-commit-cppcheck
* 
