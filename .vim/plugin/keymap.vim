" See https://vim.fandom.com/wiki/Mapping_fast_keycodes_in_terminal_Vim
" See termcap-options for mapped key info
" Some may not mapped correctly mapped for F1 to F4 (See vt100-function-keys )
" To set them, you can have a look at https://vim.fandom.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_2)
" :set <S-F1>=<press Ctrl-V> <press S-F1><press esc>
" Those are the key for gnome-terminal with zsh on archlinux
if &term =~ "xterm."
        set <S-F1>=[1;2P
        set <S-F2>=[1;2Q
        set <S-F3>=[1;2R
        set <S-F4>=[1;2S
endif
"you could also direclty map them
" map <press ctrl-V><press your key map> :some vimcmds <press Ctrl-V><press
" enter>
" you can check the result with :map <press ctrl-V><press your key map>

" MapFastKeycode: helper for fast keycode mappings
" makes use of unused vim keycodes <[S-]F15> to <[S-]F37>
function! <SID>MapFastKeycode(key, keycode)
    if s:fast_i == 46
        echohl WarningMsg
        echomsg "Unable to map ".a:key.": out of spare keycodes"
        echohl None
        return
    endif
    let vkeycode = '<'.(s:fast_i/23==0 ? '' : 'S-').'F'.(15+s:fast_i%23).'>'
    exec 'set '.vkeycode.'='.a:keycode
    exec 'map '.vkeycode.' '.a:key
    let s:fast_i += 1
endfunction
let s:fast_i = 0

call <SID>MapFastKeycode('<C-F1>', "[1;5P")
call <SID>MapFastKeycode('<C-F2>', "[1;5Q")
call <SID>MapFastKeycode('<C-F3>', "[1;5R")
call <SID>MapFastKeycode('<C-F4>', "[1;5S")
