"""""""""""""""""""""""""""""""""""""""""""
"		Language
"""""""""""""""""""""""""""""""""""""""""""
" Using dictd
function! Translate()
	let word = expand("<cword>")
	let output = system("echo -n $(dict -d eng-fra -f " .word. " 2> /dev/null | tail -n +3 | head -n -1 | sed '1s/$/:/;2,$s/$/;/')")
	if output != ""
		echohl WarningMsg
		echo output
		echohl None
	else
		echohl WarningMsg
		echo "No translation found"
		echohl None
	endif
endfunction
"default Leader is \
nnoremap <silent> <Leader>t :call Translate()<cr> 

" to enable spellchecking :set spell"
" c.f. :help spell
" ]s search next misspelled
" [s search previous misspelled
" z= suggestion
" trad files should be under /usr/share/vim/vimfiles/spell
" or .vim/spell
" they can be download at http://ftp.vim.org/vim/runtime/spell/ 
set spell spelllang=en
set nospell
