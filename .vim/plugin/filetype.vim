augroup filetypedetect
    au BufNewFile,BufRead .tmux.conf*,tmux.conf* setf tmux
augroup END


augroup filetypedetect
	" Mail
	autocmd BufRead,BufNewFile *mutt-*              setfiletype mail
augroup END
