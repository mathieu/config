#source ~/config/gdb/peda/peda.py
#source ~/config/gdb/gdb-dashboard/.gdbinit
#source ~/config/gdb/gdb_gef/gef.py

source ~/config/gdb/commands.py
source ~/config/gdb/commands2.py
source ~/config/gdb/gdb-dashboard/.gdbinit

#source ~/config/gdb/gdbinit/gdbinit
#Show OpenCV Mat with "cv_imshow imageMat"
source ~/config/gdb/imshow/cv_imshow.py
define exit
kill
quit
end

define xxd
  dump binary memory /tmp/dump.bin $arg0 ((char *)$arg0)+$arg1
  shell xxd /tmp/dump.bin
end
document xxd
  Runs xxd on a memory ADDR and LENGTH

  xxd ADDR LENTH
end

macro define offsetof(t, f) &((t *) 0)->f

set history save on
set history size 10000
set history filename ~/.gdb_history
